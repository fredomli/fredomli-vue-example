module.exports = {
    base: '/doc/', // 设置根路径
    title: 'fredomli-docs', // 网站标题
    description: 'Personal Technology website', // 描述
    themeConfig: {
        // logo
        // logo: '/logo.png',
        // 导航栏，增添修改
        nav: [
            {
                text: 'Languages', ariaLabel: 'Language Menu', items: [
                    { text: 'Chinese', link: '/language/chinese/' },
                    { text: 'English', link: '/language/english/' }
                ]
            },
            { text: 'Home', link: '/' },
            { text: 'Guide', link: '/guide/', target: '_self' },
            { text: 'fredomli', link: 'https://gitee.com/fredomli/', target: '_blank', rel: '' },
        ],
        navbar: true, // 禁用导航栏 ture禁用 | false不禁用
        // sidebar: 'auto' // 自动生成侧边栏
        sidebar: [
            ['/', 'Vuepress教程'],
            ['/guide/markdown-doc.md', 'Markdown教程'],
            ['/guide/node.md', 'Node教程'],
            // {
            //     title: 'Group 1',   // 必要的
            //     path: '/foo/',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
            //     collapsable: false, // 可选的, 默认值是 true,
            //     sidebarDepth: 1,    // 可选的, 默认值是 1
            //     children: [
            //         '/'
            //     ]
            // },
            // {
            //     title: 'Group 2',
            //     children: [ /* ... */],
            //     initialOpenGroupIndex: -1 // 可选的, 默认值是 0
            // }

            // 多个侧边栏，注意更改sidebar{}|[]
            // '/foo/': [
            //     'token'
            // ],
            // '/bar/': [
            //     'mysql'
            // ],
            // '/': [
            //     ''
            // ],
            // '/zh/': {
            //     sidebar: 'auto' // 为特定语言生成侧边栏
            // }
        ],
        // 显示所有页面的链接
        displayAllHeaders: true, // 默认值：false
        // 活动的标题栏，当用户滑动鼠标时，标题栏跟着一起变化
        activeHeaderLinks: true, // 默认值：true   
        search: true,  // 通过这个参数来禁用或者开启搜索框
        searchMaxSuggestions: 10, // 调整搜索框显示搜索记录数量
        lastUpdated: 'Last Updated', // 最后更新时间，默认是关闭的
        // 一篇和下一篇文章的链接
        nextLinks: true, // 下一篇，默认是开启的，禁止设置false
        prevLinks: true, // 上一篇，默认是开启的，禁止是指false

        // Git 仓库和编辑链接
        // 假定是 Gitee，同时也可以是一个完整的 GitLab URL
        repo: 'https://gitee.com/fredomli/fredomli-vue-example/',
        repoLabel: 'Gitee',

        // 假如你的文档仓库和项目本身不在一个仓库：
        // docsRepo: 'vuejs/vuepress',
        // 假如文档不是放在仓库的根目录下：
        docsDir: 'fredomli-vuepress-starter/docs/',
        // 假如文档放在一个特定的分支下：
        docsBranch: 'vue-press',
        // 默认是 false, 设置为 true 来启用
        editLinks: true,
         // 默认为 "Edit this page"
        editLinkText: '帮助我们改善此页面！',
        // smoothScroll: false, // 启动页面滚动效果
    },
    // 是否显示行号
    markdown: {
        lineNumbers: true
    }
};