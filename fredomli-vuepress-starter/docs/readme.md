<!-- ---
sidebar: auto //自动生成侧边栏 false禁用侧边栏
--- -->
# 快速上手搭建个人博客或个人技术网站

## 1.VuePress
VuePress由两部分组成：第一部分是一个极简静态网站生成器，它包含由Vue驱动的主题系统和插件API，另一个部分是为书写技术文档而优化的`默认主题`，它的诞生是为了支持Vue及其子项目的文档需求。  
每个由VuePress生成的页面都带有渲染好的HTML，也因此具有非常好的加载性能和搜索引擎优化`（SEO）`。同时，一旦页面被加载，Vue将接管这些静态内容，并将其转换成一个完整的单页应用`（SPA）`，其他的页面则会只在用户浏览到的时候才按需加载。  
事实上一个VuePress网站是一个由Vue、Vue Router、和Webpack驱动的单页应用。如果使用过Vue的话，当你开发一个自定义主题的时候，你会感受到非常熟悉的开发体验，甚至可以使用Vue DevTools去调试你的自定义主题。  
在构建时，我们会为应用创建一个服务端渲染`（SSR）`的版本，然后通过虚拟访问每一条路径来渲染对应的HTML，这种做法来源于Nuxt的`nuxt generate`命令。  

### 1.1 Features  
**内置的Markdown扩展**  


### 1.2快速上手  

<b style='color:red'>*前提条件:Vue需要Node.js>=8.6*</b>  
下面会从头搭建一个简单的VuePress文档。如果你想在一个现有的项目中使用VuePress管理文档，从步骤3开始。  
#### 1.创建并进入一个新目录  
```bash
mkdir vuepress-starter && cd vuepress-starter
```
#### 2.使用你喜欢的包管理器进行初始化  
```bash
npm init
```
#### 3.将VuePress安装为本地依赖  
不在推荐全局安装VuePress  
```bash
npm install -D vuepress
```
> 注意
> 如果你的现有项目依赖了 webpack 3.x，我们推荐使用 Yarn (opens new window)而不是 npm 来安装 VuePress。因为在这种情形下，npm 会生成错误的依赖树。  

#### 4.创建第一篇博文  
```bash
mkdir docs/README.md
```

#### 5.在 `package.json` 中添加一些 `scripts`  
这一步骤是可选的，但我们推荐你完成它。在下文中，我们会默认这些 scripts 已经被添加。  
```json
{
  "scripts": {
    "docs:dev": "vuepress dev docs",
    "docs:build": "vuepress build docs"
  }
}
```
#### 6.启动服务器  
```bash
npm run docs:dev
```

`VuePress` 会在 `http://localhost:8080` (opens new window)启动一个热重载的开发服务器。  

现在，你应该已经有了一个简单可用的 `VuePress` 文档。接下来，了解一下推荐的 目录结构 和 `VuePress` 中的 基本配置。  

当你的文档逐渐成型的时候，不要忘记 `VuePress` 的 多语言支持 并了解一下如何将你的文档 部署 到任意静态文件服务器上。  
### 2.目录结构  
`VuePress` 遵循 “约定优于配置” 的原则，推荐的目录结构如下：  
```bash
.
├── docs
│   ├── .vuepress (可选的)
│   │   ├── components (可选的)
│   │   ├── theme (可选的)
│   │   │   └── Layout.vue
│   │   ├── public (可选的)
│   │   ├── styles (可选的)
│   │   │   ├── index.styl
│   │   │   └── palette.styl
│   │   ├── templates (可选的, 谨慎配置)
│   │   │   ├── dev.html
│   │   │   └── ssr.html
│   │   ├── config.js (可选的)
│   │   └── enhanceApp.js (可选的)
│   │ 
│   ├── README.md
│   ├── guide
│   │   └── README.md
│   └── config.md
│ 
└── package.json
```
注意：留意目录的大写  
* `docs/.vuepress`: 用于存放全局的配置、组件、静态资源等。 
* `docs/.vuepress/components`: 该目录中的 Vue 组件将会被自动注册为全局组件。  
* `docs/.vuepress/theme`: 用于存放本地主题。  
* `docs/.vuepress/styles`: 用于存放样式相关的文件。  
* `docs/.vuepress/styles/index.styl`: 将会被自动应用的全局样式文件，会生成在最终的 CSS 文件结尾，具有比默认样式更高的优先级。  
* `docs/.vuepress/styles/palette.styl`: 用于重写默认颜色常量，或者设置新的 stylus 颜色常量。  
* `docs/.vuepress/public`: 静态资源目录。  
* `docs/.vuepress/templates`: 存储 HTML 模板文件。  
* `docs/.vuepress/templates/dev.html`: 用于开发环境的 HTML 模板文件。  
* `docs/.vuepress/templates/ssr.html`: 构建时基于 Vue SSR 的 HTML 模板文件。  
* `docs/.vuepress/config.js`: 配置文件的入口文件，也可以是 YML 或 toml。  
* `docs/.vuepress/enhanceApp.js`: 客户端应用的增强。  

注意  

当你想要去自定义 templates/ssr.html 或 templates/dev.html 时，最好基于 默认的模板文件 (opens new window)来修改，否则可能会导致构建出错。 

::: danger STOP
危险区域，禁止通行
:::

::: details 点击查看代码
```js
console.log('你好，VuePress！')
```
:::

``` js
export default {
  name: 'MyComponent',
  // ...
}
```

``` js {4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```

``` js{1,4,6-7}
export default { // Highlighted
  data () {
    return {
      msg: `Highlighted!
      This line isn't highlighted,
      but this and the next 2 are.`,
      motd: 'VuePress is awesome',
      lorem: 'ipsum',
    }
  }
}
```