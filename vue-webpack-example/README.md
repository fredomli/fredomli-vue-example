# vue-webpack-example

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


Markdown教程
============

## Markdown标题

<!-- _markdown标题有两种格式_ -->
*markdown标题有两种格式*  

1、使用=和-标记一级和二级标题
--------------------------
```
我展示的是一级标题
================

我展示的是二级标题
----------------
```

2、使用#号标记
-------------

*使用#号可表示1-6级标题，一级标题对应一个#号，以此类推。*  

```
# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题
```
## Markdown段落
Markdown 段落没有特殊的格式，直接编写文字就好，段落的换行是使用两个上的空格加上回车。  

```
Study vue and markdown  末尾添加两个空格
Study Java and C++ program  
```
当然也可以在段落后面使用一个空行来表示重新开始一个项目  
```
Study vue and markdown  末尾添加两个空格

Study Java and C++ 
```

### 字体
Markdown 可以使用以下几种字体  
```
*斜体文本*
```
示例: *斜体文本*  

```
_斜体文本_
```
示例: _斜体文本_  

```
**粗体文字**
```
示例: **粗体文字**  

```
__粗体文字__
```
示例: __粗体文字__  

```
***粗斜体文字***
```
示例: ***粗斜体文字***  

```
___粗斜体文字___
```
示例: ___粗斜体文字___  

### 分隔线
你可以在一行中用三个以上的星号、减号、底线来建立一个分隔线，线内不能有其他东西。你也可以在星号或是减号中间插入空格。下面每种写法都可以建立分割线：  
```
***

* * *

*****

- - -

----------
```
显示效果如下：  

***
* * *
*****
- - -
----------

### 删除线
如果段落上的文字要添加删除线，只需要在文字的两端加上两个波浪线~~即可，实例如下： 

```
Study vue and markdown
~~Study vue and markdown~~
```
显示效果如下：  
Study vue and markdown  
~~Study vue and markdown~~  
### 下划线
下划线可以通过HTML的 `<u>` 标签来实现  
```
<u>带下划线文本</u>
```
显示效果： <u>带下划线文本</u>

### 脚注
脚注是对文本的补充说明。  
Markdown脚注的格式如下：
```
[^要注明的文本]
```
以下实例演示了脚注的用法：  
```
Study vue and [^markdown]  

[^markdown]:是一种轻量级标记语言。  
```
演示效果如下：
Study vue and [markdown]  

[markdown]:是一种轻量级标记语言  

## Markdown列表
Markdown支持有序列表和无序列表  
无序列表使用(`*`)、加号(`+`)或是减号(`-`)作为列表标记，这些标记后面要添加一个空格，然后再填写内容：  
```
* 第一项
* 第二项
* 第三项

+ 第一项
+ 第二项
+ 第三项

- 第一项
- 第二项
- 第三项
```

显示结果如下：
* 第一项
* 第二项
* 第三项

+ 第一项
+ 第二项
+ 第三项

- 第一项
- 第二项
- 第三项

有序列表使用数字并加上`.`号来表示，如：  
```
1. 第一项
2. 第二项
3. 第三项
```
显示结果如下：  
1. 第一项
2. 第二项
3. 第三项

### 列表嵌套
列表嵌套只需在子列表中的选项前面添加四个空格即可：  
```
1. 第一项
  - 第一项嵌套的第一个元素
  - 第一项嵌套的第二个元素

2. 第二项
  - 第二项嵌套的第一个元素
  - 第二项嵌套的第二个元素
```
显示结果如下：  

1. 第一项
   - 第一项嵌套的第一个元素
   - 第一项嵌套的第二个元素

2. 第二项
   - 第二项嵌套的第一个元素
   - 第二项嵌套的第二个元素

## Markdown 区块
Markdown区块引用是在段落开头使用`>`符号，然后在后面紧跟一个**空格**符号  
```
> 区块引用一  
> 区块引用二  
> 区块引用三  
```
显示结果如下:  
> 区块引用一  
> 区块引用二  
> 区块引用三  

另外区块是可以嵌套的，一个`>`符号是最外层，两个`>>`符号是第一层嵌套，以此类推：  
```
> 最外层
>> 第一层嵌套
>>> 第二层嵌套
```
显示结果如下：  
> 最外层
> > 第一层嵌套
> > > 第二层嵌套

### 区块中使用列表

区块中使用列表实例如下：  
```
> 区块中使用列表
> 1. 第一项  
> 2. 第二项  
> + 第一项  
> + 第二项  
> + 第三项  
```

显示结果如下：  
> 区块中使用列表
> 1. 第一项
> 2. 第二项
> + 第一项
> + 第二项
> + 第三项

### 列表中是用区块
如果要在列表中使用区块，那么就需要在`>`前添加四个空格缩进。  
列表中使用区块如下：  
```
* 第一项
    > 第一项区块一
    > 第一项区块二
* 第二项
    > 第二项区块一
    > 第二项区块二
```
使用结果如下：  
* 第一项
    > 第一项区块一  
    > 第一项区块二  
* 第二项
    > 第二项区块一  
    > 第二项区块二 

## Markdown 代码
如果是段落上的一个函数或片段的代码可以用反引号把它包起来(`` ` ``),例如：  
```
`printf()` 函数
```
显示结果如下：  
`printf()` 函数  

### 代码区块
代码区块使用**4个空格**或者一个**制表符（Tab键）**。  
实例如下:  
```
    public class TestJava{
        public static void main(String[] args){
            System.out.println("Hello world");
        }
    }
```

显示结果如下：  

    public class TestJava{
        public staic void main(String[] args){
            System.out.println("Hello world");
        }
    }

你也可以使用(`` ``` ``)包裹一段代码，并指定一种语言(也可以不指定)： 

```
    ```java
    public class TestJava{
        public static void main(String[] args){
            System.out.println("Hello World");
        }
    }
    ```
```

显示结果如下：  

```java
public class TestJava{
    public static void main(String[] args){
        System.out.println("Hello World");
    }
}
```
## Markdown 链接
链接使用方法如下：  
```
[链接名称](链接地址)

或者

<链接地址>
```
例如：  
```
这是一个链接[gitee](https://www.gitee.com/fredomli)
```
显示结果如下：  
这是一个连接[gitee](https://www.gitee.com/fredomli)  

直接使用连接地址：  
```
<https://www.gitee.com/fredomli>
```
显示如下：  
<https://www.gitee.com/fredomli>  

### 高级链接  
我们可以通过变量来设置一个链接，变量赋值在文档尾部进行。  

```
这个链接用 1 作为网址变量[git][1]  

然后在文档的结尾为变量赋值（网址）

[1]: http://www.gitee.com/fredomli
```
显示结果如下：  
这个链接用 1 作为网址变量[git][1]  

[1]: http://www.gitee.com/fredomli

## 图片
### Markdown 图片
Markdown 图片语法格式如下：
```
! [alt 属性文本](图片地址)  

！[alt 属性文本](图片地址 "可选标题")  
```
+ 开头一个感叹号!
+ 接着一个方括号，里面放上图片的代替文字
+ 接着一个普通括号，里面放上图片的网址，最后还可以用引号包住并加上选择性的'title'属性文字。  

使用实例：  

![Git 图标](./src/assets/logo.png)  

当然，也可以像网址那样对图片网址使用变量：  

```
这个链接用 2 表示[Git 图标][2]  
然后在文档的结尾为变量赋值  

[2]: /src/assets/logo.png  
```
显示结果如下：  
这个链接用 2 表示[Git 图标][2].

[2]: /src/assets/logo.png  

Markdown还没有办法指定图片的高度与宽度，如果你需要的话，你可以使用普通的`<img>`标签。  
```
<img src="./src/assets/logo.png" width="50px"></img>
```
显示结果如下：  
<img src="./src/assets/logo.png" width="100px"/>  

## Markdown 表格

Markdown 制作表格使用`|`来分隔表头和其他行。  
语法如下：  
```
| 表头 | 表头 |
| ---- | ---- |
|单元格|单元格|
|单元格|单元格|
```

以上代码显示如下：  
| 表头 | 表头 |
| ---- | ----- |
| 单元格 | 单元格 |
| 单元格 | 单元格 |

### 我们可以设置表格的对齐方式：
+ `-:`设置内容和标题栏居右对齐。
+ `:-`设置内容和标题栏居左对齐。
+ `:-:` 设置内容和标题栏居中对齐。

实例如下：  
```
| 左对齐 | 右对齐 | 居中对齐 |
| :----  | ----: | :----:  |
| 单元格 | 单元格 | 单元格   |
| 单元格 | 单元格 | 单元格   |
```

以上代码显示结果如下：  
| 左对齐 | 右对齐   | 居中对齐 |
| :----  | ----:   | :----:  |
| 单元格 | 单元格   | 单元格   |
| 单元格 | 单元格   | 单元格   |

## Markdown 高级技巧
### 支持HTML元素  
不在Markdown涵盖范围之内的标签，都可以直接在文档里面用HTML撰写。  

### 转义
Markdown使用了很多特殊符号来表示特定的含义，如果需要显示特定的符号则需要使用转义字符，Markdown使用反斜杠转义特殊字符：  

```
**粗体字**  
\*\* 正常字体 \*\*
```
效果如下：  
> **粗体字**  
> \*\* 正常字体 \*\*  

Markdown支持以下这些符号前面加上反斜杠来帮助插入普通的符号：  
```
\   反斜杠  
`   反引号  
*   星号
_   下划线  
{}  花括号  
[]  方括号  
()  小括号  
#   井符号
+   加号  
-   减号  
,   英文句点  
!   感叹号
```

### 公式
当你需要在编辑器中插入数学公式时，可以使用两个美元符 $$ 包裹 TeX 或 LaTeX 格式的数学公式来实现。  

$$
  2x^2 + 4x -2 = 0
$$
