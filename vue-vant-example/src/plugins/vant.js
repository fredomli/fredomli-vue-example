import Vue from 'vue'
import {Button, Col, Row} from 'vant'
import 'vant/lib/index.css'

Vue.use(Button)
Vue.use(Col)
Vue.use(Row)
