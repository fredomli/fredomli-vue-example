import { post, get } from '../axios/axios.js'// 引入封装的文件中的post方法

export default {
  // 登录接口
  shopLogin (params) {
    return post('/webapp/ap-user/user-login', params)
  },
  // 发现好货模块-商品列表
  findGoodProduct (params) {
    return get('/webapp/ap-product/find-good-product', params)
  },
  // 商品详情接口
  findOneProduct (params) {
    return get('/webapp/ap-product/get-one-product', params)
  },
  // 支付接口
  apToPay (params) {
    return post('/webapp/ap-pay/pay-test', params)
  }
}
