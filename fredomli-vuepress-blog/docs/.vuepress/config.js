module.exports = {
    base: '/blog/', // 设置根路径
    title: 'fredomli-blog', // 网站标题
    description: 'Personal Technology blog', // 描述
    // 使用的主题 vuepress-theme-reco
    theme: 'reco',
    // 主题设置
    themeConfig: {
        type: 'blog', // 设置使用博客主题，默认是偏向文档的主题
        author: 'lishuang', // 设置全局作者
        huawei: false, // 华为文档
        logo: '/actor.png', // 网站图标
        authorAvatar: '/actor.png', // 设置首页头像
        mode: 'auto', // 默认 auto，auto 跟随系统，dark 暗色模式，light 亮色模式
        modePicker: true, // 默认 true，false 不显示模式调节按钮，true 则显示
        // 博客设置
        blogConfig: {
            category: {
                location: 2, // 再导航栏菜单中所占的位置，默认2
                text: 'Category'
            },
            tag: {
                location: 3, // 在导航菜单栏中的位置默认3
                text: 'Tag'
            },
            timeline: {
                location: 4,
                text: 'timeline'
            }
        },
        // 导航栏设置
        nav: [
            { text: 'timeline', link: '/timeline/', icon: 'reco-date' },
        ],
        friendLink: [
            {
                title: 'fredomli-doc',
                desc: 'A simple and beautiful vuepress Blog & Doc theme.',
                logo: "/actor.png",
                link: 'https://vuepress-theme-reco.recoluan.com'
            },
            {
                title: 'Vue-press',
                desc: 'This is the official vUE website',
                logo: "/logo.png",
                email: 'fredomli@163.com',
                link: 'https://vuepress.vuejs.org/'
            },
            {
                title: 'Vue.js',
                desc: 'Vue教程',
                logo: "/logo.png",
                email: 'fredomli@163.com',
                link: 'https://www.vuejs.com'
            },
        ],
        // 移动端优化
        head: [
            ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no' }]
        ],
        // 评论
        // valineConfig: {
        //     showComment: true,
        //     appId: '123456',// your appId
        //     appKey: '123456', // your appKey
        // },
        // 密文
        keyPage: {
            keys: ['e10adc3949ba59abbe56e057f20f883e'], // 1.3.0 版本后需要设置为密文
            color: '#42b983', // 登录页动画球的颜色
            lineColor: '#42b983' // 登录页动画线的颜色
        }
    }
} 