---
title: Node-基础
date: 2018-12-29
subSidebar: 'auto'
categories:
 - web
 - server
tags:
 - node
keys:
 - 'e10adc3949ba59abbe56e057f20f883e'
---
# node教程

## 1. 项目简介
* `app.js`入口文件
* `package.json`包管理文件
* `package-lock.json`项目配置文件、版本管理文件  
* `readme.md`说明文件  
* `routes`路由文件夹，该文件中配置了项目需要的路由控制
* `public`公共文件夹，一般储存一些静态文件，如`html`和`image`等文件 
* `node_modules`这是依赖文件夹，该文件中存储的是项目需要的各种插件、中间件、依赖等等。
## 2. Node.js简介  
Node.js是一个开源与跨平台的JavaScript运行时环境。它是一个可以用于几乎任何项目的工具。  
Node.js在浏览器外运行V8 JavaScript引起。 
Node.js应用程序运行于单个进程中，无需为每个请求创建新线程。Node.js中提供了一组异步I/O原生功能，以防止JavaScript代码被阻塞，并且Node.js中的库通常是使用非阻塞范式编写的。  
Node.js具有独特的优势，因为为浏览器编写JavaScript代码的开发者除了客户端代码外，还能够开发`服务端代码`。  
在Node.js中可以随便使用新的ECMAScript标准，因为不必等待所有用户更新浏览器，可以通过更改Node.js的版本来决定使用ECMAScript的版本。  



### 2.1 Node应用程序示例  

Node.js最常见的Web `Hello world`示例。  
```javascript
const http = require('http')

const hostname = '127.0.0.1'
const port = 3000

const server = http.createServer((req, res) =>{
    res.statusCode = 200
    res.setHeader('Content-type','text/plain','charset=utf-8')
    res.end('你好世界\n')
})

server.listen(port, hostname, () => {
    console.log('服务器正在http://${hostname}:${port}/')
})
```
执行此代码首先需要引入，<b style="color:green;">`http`</b>模块  
<b style="color:green;">`http`</b>的`createServer()`方法会创建新的http服务器并返回。  
每当接收到新的请求时，request 事件会被调用，并提供两个对象：一个请求（http.IncomingMessage 对象）和一个响应（http.ServerResponse 对象）。

这两个对象对于处理 HTTP 调用至关重要。

第一个对象提供了请求的详细信息。 在这个简单的示例中没有使用它，但是你可以访问请求头和请求数据。

第二个对象用于返回数据给调用方。  
 **解决请求乱码问题**：
```javascript
res.setHeader('Content-Type', 'text/plain;charset=UTF-8')
```

设置响应头：
```javascript
res.setHeader('Content-Type', 'text/plain')
```

关闭响应：  
```javascript
res.end('你好世界\n')
```
### 2.2 Node.js框架和工具  

* `Adonisjs`:一个全栈框架，高度专注于开发者的效率、稳定和信任。 Adonis 是最快的 Node.js Web 框架之一。  
* `Express` :提供了创建 Web 服务器的最简单但功能最强大的方法之一。 它的极简主义方法，专注于服务器的核心功能，是其成功的关键。  
* `Fastify`:一个 Web 框架，高度专注于提供最佳的开发者体验（以最少的开销和强大的插件架构）。 Fastify 是最快的 Node.js Web 框架之一。  
* `hapi`:一个富框架，用于构建应用程序和服务，使开发者可以专注于编写可重用的应用程序逻辑，而不必花费时间来搭建基础架构。  
* `koa`:由 Express 背后的同一个团队构建，旨在变得更简单更轻巧。 新项目的诞生是为了满足创建不兼容的更改而又不破坏现有社区。  
* `NestJS`:一个基于 TypeScript 的渐进式 Node.js 框架，用于构建企业级的高效、可靠和可扩展的服务器端应用程序。  
* `Next.js`:用于渲染服务器端渲染的 React 应用程序的框架。
* `Nx`:使用 NestJS、Express、React、Angular 等进行全栈开发的工具包！ Nx 有助于将开发工作从一个团队（构建一个应用程序）扩展到多个团队（在多个应用程序上进行协作）！  
* `Socket.io`:一个实时通信引擎，用于构建网络应用程序。  

## 3. Node.js基本命令  
### 3.1 在命令行运行Node.js脚本  
运行 Node.js 程序的常规方法是，运行全局可用的 node 命令（已安装 Node.js）并传入要执行的文件的名称。  

比如主 Node.js 应用程序文件是 app.js，则可以通过键入以下命令调用它：  

```javascript
node app.js
```
当运行命令时，请确保位于包含 app.js 文件的目录中。  

### 3.2 如何从Node.js程序退出  
有很多种方法可以终止 Node.js 应用程序。

当在控制台中运行程序时，可以使用 `ctrl-C` 将其关闭，但是这里要讨论的是以编程的方式退出。  

`process` 核心模块提供了一种便利的方法，可以编程方式退出Node.js程序：`prosess.exit()` 当 Node.js 运行此行代码时，进程会被立即强制终止。  

可以传入一个整数，向操作系统发送退出码：  
```javascript
prosess.exit(1)
```
默认情况下，退出码为 0，表示成功。 不同的退出码具有不同的含义，可以在系统中用于程序与其他程序的通信。  

也可以设置 process.exitCode 属性： 
```javascript
process.exitCode = 1
```

使用 Node.js 启动服务器，例如 HTTP 服务器：  
```javascript
const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.send('你好')
})

app.listen(3000, () => console.log('服务器已就绪'))
```
这个程序永远不会结束。 如果调用 process.exit()，则任何当前等待中或运行中的请求都会被中止。 这不太友好。  
**注意**:`process`是不需要"require",它是自动可用的。  

在这种情况下，需要向该命令发送SIGTERM信号，并使用进程的信号处理程序进行处理： 
```javascript
const express = require('express')

const app = express()

app.get('/',(req, res) => {
    res.send('你好世界')
})

const server = app.listen(3000, () => {
    console.log("服务器已经就绪")
})

process.on('SIGTERM',() => {
    server.close(() => {
        console.log('进程已终止')
    })
})
```
SIGKILL 是告诉进程要立即终止的信号，理想情况下，其行为类似于 process.exit()。

SIGTERM 是告诉进程要正常终止的信号。它是从进程管理者（如 upstart 或 supervisord）等发出的信号。  
可以从程序内部另一个函数中发送此信号：  
```javascript
process.kill(process.pid, 'SIGTERM')
```

### 3.3 如何从Node.js读取环境变量  
Node.js的核心模块`process`提供了`env`属性，该属性承载了在启动进程时设置的所有环境变量。  
示例：
```javascript
process.env.NODE_ENV  // "development"
```

在脚本运行之前将其设置为 "production"，则可告诉 Node.js 这是生产环境。  
可以用相同的方式访问设置的任何自定义的环境变量。  

### 3.4 如何使用Node.js  
<b style="color:red;">`node`</b>命令是用来运行Node.js 脚本的命令。  

```javascript
node app.js
```

如果省略文件名，则在REPL模式中使用它：  
```javascript
node
```
> 注意：REPL 也被称为运行评估打印循环，是一种编程语言环境（主要是控制台窗口），它使用单个表达式作为用户输入，并在执行后将结果返回到控制台。  

如果在终端中尝，试则会出现如下：  
```javascript
>node
>
```

该命令保持空闲状态，并等待输入内容。

确切的说，REPL 正在等待输入一些JavaScript代码。

从简单开始，输入：
```console
>console.log('测试')
>测试
>undefined
>
```
第一个值`测试`是告诉控制台要打印输出，然后得到`undefined`,它是运行`console.log()`的返回值。  

#### 3.4.1 使用Tab键自动补全  
REPL是交互式的。在编写代码的时候，如果按下`tab`键，则REPL会尝试自动补全所写的内容，以匹配已定义或预定义的变量。  

#### 3.4.2 探索JavaScript对象  
尝试输入JavaScript的类名称，例如`Number`,添加一个点号并按下`tab`。  
REPL 会打印可以在该类上访问的所有属性和方法。  

#### 3.4.3 探索全局对象  
通过输入`global`并按下`tab`,可以检查可以访问的全局变量。 

```console
> global.
```
#### 3.4.4 点命令
REPL有一些特殊的命令，所有这些命令都以点号`.`开头。它们是：  
* `.help`:显示点命令帮助。  
* `editor`:启用编辑器模式，可以轻松的编写多行JavaScript代码。当处于此模式时，按下`ctrl-D`可以运行编写的代码。  
* `.break`:当输入多行的表达式时，输入`.break`命令可以终止进一步输入。相当于按下`ctrl-c`。  
* `.clear`:将REPL上下文重置为空对象，并清除当前正在输入的任何多行表达式。  
* `.load`:加载JavaScript文件（相当于当前工作目录）  
* `.save`:将在REPL中输入的所有内容保存到文件。  
* `.exit`:退出REPL（相当于按下两次ctrl-c） 

### 3.5 Node.js从命令行接收参数
当使用以下命令调用Node.js应用程序时，可以传入任意数量的参数：  
```javascript
node app.js
```
参数是可以独立的，也可以是键和值。
例如：  
```javascript
node app.js zhangsan
```
或
```javascript
node app.js name=zhangsan
```

这会改变在Node.js代码中获取参数值的方式。  
获取参数值的方式是使用Node.js中内置的`process`对象。  
它公开了`argv` 属性，该属性是一个包含所有命令行参数的数组。  
第一个参数是`node`命令的完整路径。
第二个参数是正被执行的文件的文件完整路径。  
所有其他参数从第三个位置开始。  
可以使用循环遍历所有参数。  
```javascript
process.argv.forEach((val, index) => {
  console.log(`${index}: ${val}`)
})
```

可以通过创建一个排除前面两个参数的新数组来仅获取其他参数：  
```javascript
const args = process.argv.slice(2)
```

### 3.6 使用Node.js输出到命令行  
#### 3.6.1 使用控制台模块的基础输出  
Node.js提供了`console`模块，该模块提供了大量非常有用的与命令行交互的方法。  
它基本上与浏览器中的`console`对象相同。

最基础、最常用的方法是`console.log()`,该方法会打印传入到控制台的字符串。  
如果传入对象，则会以字符串的方式呈现。

可以传入多个变量到`console.log()`,例如：  

```javascript
const x = 'x'
const y = 'y'
console.log(x,y)
```

也可以使用传入变量和格式说明符来格式化用语言。
例如：
```javascript
console.log('我的%s已经%d岁','猫',2)
```
* `%s` 会格式化变量为字符串
* `%d` 会格式化变量为数字
* `%i` 会格式化变量为其整数部分
* `%o` 会格式化变量为对象

例如:  
```javascript
console.log('%o', Number)
```

#### 3.6.2 清空控制台  
`console.clear()`会清除控制台（其行为可能取决于所使用的控制台）  
#### 3.6.3 元素计数
`console.count()` 使用方式如下：  
```javascript
const x = 1
const y = 2
const z = 3
console.count(x)
console.count(x)
console.count(y)
console.count(y)
console.count(z)
console.count(z)

```

count 方法会对打印的字符串的次数进行计数，并在其旁边打印计数：  
数苹果和橙子：  

```javascript
const orange = ['橙子','橙子']
const apples = ['苹果']
orange.forEach(fruit => {
    console.log(fruit)
})
apples.forEach(fruit => {
    console.log(fruit)
})
```
#### 3.6.4 打印堆栈信息  

在某些情况下，打印函数的调用堆栈踪迹很有用。  
```javascript
const function2 = () => {
    console.trace()
}
const function1 = () => function2()
function1()
```

#### 3.6.5 计算耗时  
可以使用`time()`和`timeEnd()`轻松的计算函数运行所需的时间：  

```javascript
const doSomething = () => console.log('测试')
const measureDoingSomething = () => {
  console.time('doSomething()')
  //做点事，并测量所需的时间。
  doSomething()
  console.timeEnd('doSomething()')
}
measureDoingSomething()
```

**`stdout`和`stderr`**

console.log非常适合在控制台打印消息。这就是所谓的标准输出（或称为`stdout`） 
`console.error`会打印到`stderr`流  
它不会出现在控制台中，但是会出现在错误日志中。 

#### 3.6.6 为输出着色  
可以使用转义序列在控制台中为文本的输出着色，转义字符是一组标识颜色的字符。  
```javascript
console.log('\x1b[33m%s\x1b[0m', '你好')
```
可以在Node.js REPL中进行尝试，它会打印出黄色的<b style="color:orange;">`你好`</b>。  

以上是底层方法。为控制台输出着色的最简单的方法是使用库。<b style="color:green;">`Chalk`</b>是这样一个库，除了着色外，它还有助于其他样式的设置（例如使文本变粗、斜体和下划线等等）  

使用<b style="color:red;">`npm install chalk`</b>进行安装，然后就可以使用它：  

``` javascript
const chalk = require('chalk')
console.log(chalk.yellow('你好'))
console.log(chalk.red('你好'))
```

与尝试记住转义代码相比，使用 chalk.yellow 方便得多，并且代码更具可读性。  

#### 3.6.7 创建进度条
<b style="color:green;">**Progress**</b>是一个很棒的软件包，可以在控制台中创建进度条。使用<b style="color:red;">`npm install progress`</b>进行安装。  
以下示例会创建一个10步的进度条，每100毫秒完成一步。当进度条结束时，则清除定时器：  

```javascript
const ProgressBar = require('progress')

const bar = new ProgressBar(':bar', { total: 10 })
const timer = setInterval(() => {
  bar.tick()
  if (bar.complete) {
    clearInterval(timer)
  }
}, 100)
```

### 3.7 在Node.js中从命令行接受输入  
从版本7开始，Node.js提供了`readline`模块来执行以下操作：每次一行地从可读流（例如`process.stdin`流，在Node.js程序执行期间该流就是终端输入）获取输入。  

```javascript
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

readline.question(`你叫什么名字?`, name => {
  console.log(`你好 ${name}!`)
  readline.close()
})
```
这段代码会询问用户名，当用户输入姓名按下回车时，则会发送问候语句。
`question()`方法会显示第一个参数，并等待用户的输入。当按下回车键时，则它会调用回调函数。  

在此回调函数中，关闭了readline接口。  
`readline`还提供了其他几个方法。  
最简单的方式是使用<b style="color:green;">`readline-sync`</b>软件包，其在API方面非常相似。  
<b style="color:green;">`inquirer.js`</b>软件包则提供了更完整、更抽象的解决方案。  
使用 <b style="color:red;">`npm install inquirer`</b>进行安装。  
复用上例代码：
```javascript
const inquirer = require('inquirer')

var questions = [
  {
    type: 'input',
    name: 'name',
    message: "你叫什么名字?"
  }
]

inquirer.prompt(questions).then(answers => {
  console.log(`你好 ${answers['name']}!`)
})
```

Inquirer.js 可以执行许多操作，例如询问多项选择、展示单选按钮、确认等。  
所有的可选方案都值得了解，尤其是 Node.js 提供的内置方案，但是如果打算将 CLI 输入提升到更高的水平，则 Inquirer.js 是更优的选择。  

### 3.8 使用exports从Node.js文件中公开功能  

Node.js具有内置的功能模块。  
Node.js文件可以导入其他Node.js文件公开功能。  
当想要导入某些功能时：  
```javascript
const library = require('./library')
```

可以导入存在与当前文件夹中的`library.js`文件中公开的功能。  

在此文件中，必须先公开功能，然后其他文件才能将其导入。  
默认情况下，文件中定义的任何其他对象或变量都是私有的，不会公开给外界。  

这就是<b style="color:red;">`module`</b>系统提供的`module.exports` API可以做的事。  


当将对象或函数赋值为新的`exports`属性时，就是要被公开的内容，因此，可以将其导入应用程序的其他部分或其他应用程序中。  

可以通过两种方式进行操作:  
* 第一种方式是将对象赋值给`molule.exports`模块，这会使文件导出指定的对象。  
```javascript
//文件名 car.js
const car = {
    name: 'bm',
    red: 'red'
}

//导出
module.exports = car

//正在同级的另一个文件中使用
const car = require(./car)
```

* 第二种方式是将要导出的对象添加为`exports`属性。这种方式可以导出多个对象、函数或数据。  
```javascript
const car = {
  name: 'bm',
  color: 'red'
}
exports.car = car
```
或直接：
```javascript
exports.car = {
  name: 'bm',
  color: 'red'
}
```

在另一个文件中引用：
```javascript
const items = require('./car')
items.car
//或
const car = require('./car').car
```


`module.exports` 和 `export` 之间有什么区别？  
前者公开了它指向的对象。 后者公开了它指向的对象的属性。  

## 4. npm包管理器简介  
### 4.1 npm简介  
`npm`是Node.js标准的软件包管理器。  

在 2017 年 1 月时，npm 仓库中就已有超过 350000 个软件包，这使其成为世界上最大的单一语言代码仓库，并且可以确定几乎有可用于一切的软件包。  

它起初是作为下载和管理Node.js包依赖的方式，但其现在已经成为前端JavaScript中使用的工具。  

> Yarn是npm的一个代替选择

**下载**  
`npm`可以管理项目依赖的下载。

**安装所有依赖**  
如果项目具有`package.json`文件，则通过运行：  
```console
npm install
```
**创建package.json文件**  
可以使用`npm init -y`命令来生成package.json文件。 

它会在`node_modules`文件夹（如果不存在则会创建）中安装项目所需的所有东西。  

**安装单个软件包**  
使用下面的命令来安装指定的软件安装包：
```console
npm install package-name
```
通常会在此命令中看到更多标志：  
* <font color=red>`--save`</font>安装并添加条目到`package.json`文件的<b>`dependencies`</b>。  
* <font color=red>`--save-dev`</font>安装并添加条目到`package.json`文件的<b>`DevDependencies`</b>。  

区别主要是，`dependencies`通常是开发的工具（例如测试的库），而`DevDependencies`则是与生产环境中的应用程序相关。  

**更新软件包**  
```console
npm undate
```

npm会检查所有的软件包是否有满足版本限制的更新版本。  

也可以指定单个软件包进行更新：  
```console
npm update <package-name>
```
#### 4.1.1 版本控制  
除了简单的下载之外，`npm` 还可以管理版本控制，因此可以指定软件包的任何特定版本，或要求版本高于或低于所需的版本。  

很多时候，一个库仅与另一个库的主版本兼容。 

或者，一个库最新的版本中有一个缺陷引起了问题。  

指定库的显示版本还有助于使每个人都使用相同的软件包版本，以便整个团队运行相同的版本，直至`package.json`文件被更新。  

在所有这些情况中，版本控制都有很大的帮助，`npm`遵循语义版本控制标准。  

#### 4.1.2 运行任务  
`package.josn`文件支持一种用于指定命令行任务的格式：  
```console
npm run <tack-name>
```
例如：
```json
{
    "scripts": {
        "start-dev": "node lib/server-development",
        "start": "node lib/server-production"
    }
}
```
使用此特性运行Webpack是很常见的：  
```json
{
  "scripts": {
    "watch": "webpack --watch --progress --colors --config webpack.conf.js",
    "dev": "webpack --progress --colors --config webpack.conf.js",
    "prod": "NODE_ENV=production webpack -p --config webpack.conf.js",
  },
}
```

因此可以运行如下，而不是输入那些容易忘记或输入错误的长命令。  
```console
$ npm run watch
$ npm run dev
$ npm run prod
```
以上命令都在package.json文件中进行配置。  


### 4.2 npm将软件包安装到哪里  

使用`npm`安装软件包时，可以执行两种类型：  
* 本地安装
* 全局安装

默认情况下，当输入`npm install`命令时，例如：  
```console
npm install cowsay
```
软件包会被安装到当前文件树中的`node_modules`子文件夹下。  

这种情况下，`npm`还会在当前文件夹中存在的`package.json`文件的`dependencies`属性中添加`cowsay`条目。  

使用`-g`标志可以执行全局安装：  
```console
npm install -g cowsay
```

在这中情况下，`npm`不会将软件包安装到本地文件夹下，而是使用全局位置。  

如何查看全局位置： 
使用<font color=red>`npm root -g`</font>命令可以查看具体的位置。  

如果使用 nvm 管理 Node.js 版本，则该位置会有所不同。  

### 4.3 如何使用或执行npm安装的软件包   

当使用`npm`将软件包安装到`node_modules`文件夹中，或全局安装时，如何在Node.js代码中使用它？  
假设使用一下命令安装了流行的JavaScript使用工具`lodash`:  
```javascript
npm install lodash
```
这会将软件包安装到本地的`node_modules`文件夹中。  
若是要在代码中使用它，则需要使用`repuire`将其代入程序：  
```javascript
const _ = require('lodash');
```

如果软件包是可执行文件怎么办？  

这种情况下，它会把可执行文件放到`node_modules/.bin/`文件夹下。  
验证这一点的简单示例是cowsay.  
cowsay软件包提供了一个命令行程序，可以执行该程序以使母牛说话（以及其他动物也可以说话）。  
当使用`npm install cowsay`安装软件包时，它会在node_mudules文件夹中安装自身以及一些依赖包：  


如果运行这些文件？  
可以输入`./node_modules/.bin/cowsay`来运行它，但是最新版本的npm（5.2）中包含的npx是更好的选择。只需运行：  
```javascript
npx cowsay
```

则npx会找到程序包的位置。 

![alt cowsay](/images/cowsay.jpg)   

### 4.4 package.json指南  

如果使用JavaScript、或者曾经与JavaScript项目、Node.js或前端项目进行过交互，则肯定会遇到过`package.json`文件。 

它有什么用途？应该了解它的什么，可以使用它完成那些有趣的事请？  

`package.json`文件是项目的清单。它可以做很多完全互不相关的事情。例如，它是用与工具的配置中心，它也是`npm`和`yarn`存储所有已安装软件包的名称和版本的地方。  

#### 4.4.1 文件结构  
这是一个示例的package.json文件：  
```javascript
{}
```

它是空的！对于应用程序，`package.json`文件中的内容没有固定要求。唯一的要求是必须遵守JSON格式，否则，尝试以编程的方式访问其属性的程序则无法读取它。

如果要构建要在`npm`上发布的Node.js软件包，则必须具有一组可帮助其他人使用它的属性。稍后会详细介绍。  

```javascript
{
  "name":"pakage_name"
}
```

它定义了`name`属性，用于告知应用程序或软件包的名称。  

这是另一个`package.json`:  
```javascript
{
  "name": "vue-node-example",
  "version": "1.0.0",
  "description": "",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node ./app"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "chalk": "^4.1.0", 
    "cowsay": "^1.4.0",
    "express": "^4.17.1",
    "inquirer": "^7.3.3",
    "progress": "^2.0.3"
  },
  "devDependencies": {

  },
  "engines": {

  }
}
```

这里面有很多的属性：  
* `name`设置了应用程序/软件包的名称。
* `version`表明了当前版本。
* `description`是应用程序/软件包的简短描述。
* `main`设置了应用程序的入口点。
* `private`如果设置为`true`,则可以防止应用程序/软件包被以外地发布到`npm`。
* `scripts`定义了一组可以运行的node脚本。
* `dependencies`设置了作为依赖安装的`npm`软件包列表。
* `devDependencies`设置了作为开发依赖安装的`npm`软件包列表。
* `engines`设置了此软件包/应用程序在那个版本的Node.js上运行。
* `browserslist`用于告知要支持那些浏览器。

以上所有的这些属性都可被`npm`或其他工具使用。  

#### 4.4.2 属性分类
本节详细介绍了可以使用的属性。  

**name**  
设置软件包名称。  
示例：  
```javascript
"name": "nodejs"
```
名称必须小于214个字节，且不能包含空格，只能包含小写字母、连字符-或下划线_。  
这是因为当软件包在`npm`上发布时，它会基于此属性获得自己的URL。  

**author**  
列出软件包的作者名称。  
示例：  
```javascript
"author": "ls"
```

也可以使用以下格式：  
```javascript
{
  "author": {
    "name": "ls",
    "email": "fredomli@163.com",
    "url": "none"
  }
}
```

**contributors**  
除作者外，该项目可以有一个或多个贡献者，此属性是列出他们的数组。  
示例：  
```javascript
{
  "contributors": ["zhangsan"]
}
```

也可以像这样：  
```javascript
{
  "contributors": [
    {
      "name": "zhansan",
      "email": "test@163.com",
      "url": "none"
    }
  ]
}
```

**bugs**  
链接到软件包的问题跟踪器，最常用的是GitHub的issues页面。  
示例：  
```javascript
"bugs": "https://github.com/nodejscn/node-api-cn/issues"
```

**homepage**  
设置软件包的主页。  
示例：  

```javascript
"homepage": "www.test.com"
```

**version**   
指定软件包当前的版本。  
示例：  
```javascript
"version": "1.0.0"
```
此属性遵循版本的语义版本控制记法，这意味着版本始终以三个数字表示：`x.x.x`。  
第一个数字是主版本号，第二个数字是次版本号，第三个数字是补丁版本。  
这些数字中的含义是：仅修复缺陷的版本是补丁版本，引入向后兼容的更改的版本是次版本，具有重大更改的是主版本。  
**license**  
指定软件包的许可证。  
示例：  
```javascript
"license": "MIT"
```

**keywords**  
此属性包含与软件包功能相关的关键字数组。  
示例：  
```javascript
"keywords": [
  "email",
  "machine learning",
  "ai"
]
```

这有助于人们在浏览相似的软件包或浏览网站时找到你的软件包。  

**description**  
此属性包含了对软件包的简短描述。  
示例：  
```javascript
"description": "这是一段描述"
```

**repository**  
此属性指定了此程序包仓库所在的位置。  
示例：  
```javascript
"repository": "github:nodejscn/node-api-cn",
```
注意`github`前缀，其他流行的服务商还包括：  
```javascript
"repository": "gitlab:nodejscn/node-api-cn",
```

 
```javascript
"repository": "bitbucket:nodejscn/node-api-cn",
```

可以显式地设置版本控制系统：

```javascript
"repository": {
  "type": "git",
  "url": "https://github.com/nodejscn/node-api-cn.git"
}
```
也可以使用其他的版本控制系统：  
```javascript
"repository": {
  "type": "svn",
  "url": "..."
}
```

**main**  
设置软件包的入口点。  
当在应用程序中导入此软件包时，应用程序会在该位置搜索模块的导出。  
示例：  
```javascript
"main": "src/main.js"
```
**private**  
如果设置为 true，则可以防止应用程序/软件包被意外发布到 npm 上。  
示例：  
```javascript
"private": true
```

**scripts**  
可以定义一组可以运行的 node 脚本。  
示例：  
```javascript
"scripts": {
  "dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",
  "start": "npm run dev",
  "unit": "jest --config test/unit/jest.conf.js --coverage",
  "test": "npm run unit",
  "lint": "eslint --ext .js,.vue src test/unit",
  "build": "node build/build.js"
}
```
这些脚本是命令行应用程序，可以通过调用`npm run xxx`或`yarn xxxx`来运行他们，其中`xxx`是命令名称，例如：`npm run dev`。  

可以为命令使用任何的名称，脚本也可以是任何操作。  

**dependencies**  
设置作为依赖安装的 npm 软件包的列表。  
当使用 npm 或 yarn 安装软件包时：  
```console
npm install <PACKAGENAME>
yarn add <PACKAGENAME>
```
该软件包会被自动地插入此列表中。  

示例：  
```javascript
"dependencies": {
  "vue": "^2.5.2"
}
```
**devDependencies**  
设置作为开发依赖安装的 npm 软件包的列表。  
它们不同于 dependencies，因为它们只需安装在开发机器上，而无需在生产环境中运行代码。  
当使用 npm 或 yarn 安装软件包时：  
```console
npm install --dev <PACKAGENAME>
yarn add --dev <PACKAGENAME>
```

该软件包会被自动地插入此列表中。

**engines**  
设置此软件包/应用程序要运行的 Node.js 或其他命令的版本。  
示例：  
```javascript
"engines": {
  "node": ">= 6.0.0",
  "npm": ">= 3.0.0",
  "yarn": "^0.13.0"
}
```
**browserslist**  
用于告知要支持哪些浏览器（及其版本）。 Babel、Autoprefixer 和其他工具会用到它，以将所需的 polyfill 和 fallback 添加到目标浏览器。  

示例：  

```console
"browserslist": [
  "> 1%",
  "last 2 versions",
  "not ie <= 8"
]
```

此配置意味着需要支持使用率超过 1％（来自 CanIUse.com 的统计信息）的所有浏览器的最新的 2 个主版本，但不含 IE8 及更低的版本。  

#### 4.4.3 命令特有的属性
package.json 文件还可以承载命令特有的配置，例如 Babel、ESLint 等。

每个都有特有的属性，例如 eslintConfig、babel 等。 它们是命令特有的，可以在相应的命令/项目文档中找到如何使用它们。  

#### 4.4.4 软件包版本  

在上面的描述中，已经看到类似以下的版本号：〜3.0.0 或 ^0.13.0。 它们是什么意思，还可以使用哪些其他的版本说明符？  

该符号指定了软件包能从该依赖接受的更新。  

鉴于使用了 semver（语义版本控制），所有的版本都有 3 个数字，第一个是主版本，第二个是次版本，第三个是补丁版本，具有以下规则：  

* `~`：如果写入的是`~0.13.0`,则只更新补丁版本: 即`0.13.1`可以，但是`0.14.0`不可以。  
* `^`：如果写入的是`^0.13.0`，则只要更新补丁版本和次版本：即`0.13.1`，`0.14.0`、以此类推。  
* `*`：如果写入的是`*`，则表示接受所有的更新，包括主版本升级。  
* `>`：接受高于指定版本的任何版本。  
* `>=`：接受等于或高于指定版本的任何版本。  
* `<=`：接受等于或小于指定版本的任何版本。  
* `<`：接受小于指定版本的任何版本。  

**还有其他的规则**  
* 无符号：仅接受指定的特定版本。  
* `latest`：使用可用的最新版本。  

还可以在范围内组合以上大部分内容，例如：`1.0.0 || >=1.1.0 <1.2.0`，即使用 1.0.0 或从 1.1.0 开始但低于 1.2.0 的版本。


### 4.5 package-lock.json文件  
在版本 5 中，npm引入了`package-lock.json`文件。  
该文件旨在跟踪被安装的每个软件包的确切版本，以便产品可以已相同的方式被100%复制（即使软件包的维护者更新了软件包）  

这解决了`package.json`一直未解决的特殊问题。在package.json中，可以使用semver表示法设置要升级到的版本(补丁版本或次版本)，例如：  

* 如果写入的是 〜0.13.0，则只更新补丁版本：即 0.13.1 可以，但 0.14.0 不可以。
* 如果写入的是 ^0.13.0，则要更新补丁版本和次版本：即 0.13.1、0.14.0、依此类推。
* 如果写入的是 0.13.0，则始终使用确切的版本。

无需将node_modules文件夹提交到Git，当尝试使用`npm install`命令在另外一台机器上复制项目时，如果指定了`~`语法并且软件包发布了补丁版本，则该软件包会被安装`^`和次版本也一样。  

因此，原始的项目和新初始化的项目实际上是不同的。 即使补丁版本或次版本不应该引入重大的更改，但还是可能引入缺陷。  
`package-lock.json`会固化当前安装的每个软件包的版本，当运行`npm install`时，`npm`会使用这些确切的版本。  
`package-lock.json`文件需要被提交到 Git 仓库，以便被其他人获取。  

当运行`npm update`时，`package-lock.json`文件中的依赖的版本会被更新。  
它们会按字母顺序被添加到文件中，每个都有 version 字段、指向软件包位置的 resolved 字段、以及用于校验软件包的 integrity 字符串。  

### 4.6 查看npm包安装的版本  
若要查看所有已安装的npm包（包括依赖包）的最新版本，则：  
```console
npm list
```

例如：
```console
❯ npm list
/Users/joe/dev/node/cowsay
└─┬ cowsay@1.3.1
  ├── get-stdin@5.0.1
  ├─┬ optimist@0.6.1
  │ ├── minimist@0.0.10
  │ └── wordwrap@0.0.3
  ├─┬ string-width@2.1.1
  │ ├── is-fullwidth-code-point@2.0.0
  │ └─┬ strip-ansi@4.0.0
  │   └── ansi-regex@3.0.0
  └── strip-eof@1.0.0
```
也可以打开`package-lock.json`文件，但这需要进行一些视觉扫描。  

`npm list -g`也是一样，但适用与全局安装的软件包  
若要仅获取顶层的软件包（基本上就是告诉npm要安装并在`package.josn`中列出的软件包），则运行`npm list --depth=0`:  

```console
❯ npm list --depth=0
/Users/joe/dev/node/cowsay
└── cowsay@1.3.1
```
也可以通过指定名称来获取特定软件包的版本：  

```console
❯ npm list cowsay
/Users/joe/dev/node/cowsay
└── cowsay@1.3.1
```
这也适用于安装的软件包的依赖：  

```console
❯ npm list minimist
/Users/joe/dev/node/cowsay
└─┬ cowsay@1.3.1
  └─┬ optimist@0.6.1
    └── minimist@0.0.10
```
如果要查看软件包在 npm 仓库上最新的可用版本，则运行`npm view [package_name] version`:  

```
❯ npm view cowsay version

1.4.0
```
### 4.7 npm包的旧版本  

可以使用`@`语法来安装npm软件包的旧版本： 

```console
npm install <package>@<version>
```
示例:  
```javascript
npm install cowsay
```
以上命令会安装1.4.0版本： 

使用以下命令可以安装1.2.0版本：  

```console
npm install cowsay@1.2.0
```
全局的软件包也可以这样做：  

```console
npm install -g webpack@4.16.4
```

可能还有需要列出软件包所有的以前版本。可以使用`npm view <package> versions`：  

```console
[
  '1.0.0', '1.0.1', '1.0.2',
  '1.0.3', '1.1.0', '1.1.1',
  '1.1.2', '1.1.3', '1.1.4',
  '1.1.5', '1.1.6', '1.1.7',
  '1.1.8', '1.1.9', '1.2.0',
  '1.2.1', '1.3.0', '1.3.1',
  '1.4.0'
]
```
### 4.8 将所有Node.js依赖包更新到最新版本  

当使用`npm install <package>`安装软件包时，该软件包最新的可用版本会被下载并放入`node_modules`文件夹中，并且还会将相应的条目添加到当前文件夹中存在的`package.json`和`package-lock.json`文件中。  

当 `npm install cowsay` 时，此条目会被添加到 package.json 文件中：  

```json
{
  "dependencies": {
    "cowsay": "^1.3.1"
  }
}
```

以下是 `package-lock.json` 的片段，为方便查看，在其中删除了嵌套的依赖：  

```json
{
  "requires": true,
  "lockfileVersion": 1,
  "dependencies": {
    "cowsay": {
      "version": "1.3.1",
      "resolved": "https://registry.npmjs.org/cowsay/-/cowsay-1.3.1.tgz",
      "integrity": "sha512-3PVFe6FePVtPj1HTeLin9v8WyLl+VmM1l1H/5P+BTTDkMAjufp+0F9eLjzRnOHzVAYeIYFF5po5NjRrgefnRMQ==",
      "requires": {
        "get-stdin": "^5.0.1",
        "optimist": "~0.6.1",
        "string-width": "~2.1.1",
        "strip-eof": "^1.0.0"
      }
    }
  }
}
```
现在，这两个文件告诉我们，已安装了 `cowsay` 的 `1.3.1` 版本，并且更新的规则是 `^1.3.1`（这对于 npm 版本控制规则意味着 npm 可以更新到补丁版本和次版本：即 `1.3.2`、`1.4.0`、依此类推）。  

如果有新的次版本或补丁版本，并且输入了 npm update，则已安装的版本会被更新，并且 package-lock.json 文件会被新版本填充。  

**注意:** package.json 则保持不变。

若要发觉软件包的新版本，则运行 `npm outdated`。  

这些更新中有些是主版本。 运行 `npm update` 不会更新那些版本。 主版本永远不会被这种方式更新，因为它们（根据定义）会引入重大的更改，`npm` 希望为你减少麻烦。  

若要将所有软件包更新到新的主版本，则全局地安装 `npm-check-updates` 软件包：  

```console
npm install -g npm-check-updates
```
然后运行：  
```console
ncu -u
```
这会升级 `package.json` 文件的 `dependencies` 和 `devDependencies` 中的所有版本，以便 npm 可以安装新的主版本。  
现在可以运行更新了：  

```console
npm update
```
如果只是下载了项目还没有 node_modules 依赖包，并且想先安装新的版本，则运行：  

```console
npm install
```

### 4.9 卸载npm软件包  

若要卸载之前在本地安装（在 node_modules 文件夹使用 `snpm install <package-name>`）的软件包，则从项目的根文件夹（包含 node_modules 文件夹的文件夹）中运行：  

```console
npm uninstall <package-name>
```
如果使用 `-S` 或 `--save` 标志，则此操作还会移除 `package.json` 文件中的引用。  

如果程序包是开发依赖项（列出在 package.json 文件的 devDependencies 中），则必须使用 `-D` 或 `--save-dev` 标志从文件中移除：  


```console
npm uninstall -S <package-name>
npm uninstall -D <package-name>
```
如果该软件包是全局安装的，则需要添加 -g 或 `--global` 标志：  

```console
npm uninstall -g <package-name>
```
例如:  
```console
npm uninstall -g webpack
```
可以在系统上的任何位置运行此命令，因为当前所在的文件夹无关紧要。  


### 4.10 npm全局和本地的软件  

本地和全局的软件包之间的主要区别是：  
* 本地的软件包 安装在运行 `npm install <package-name>` 的目录中，并且放置在此目录下的 `node_modules` 文件夹中。  
* 全局的软件包 放在系统中的单独位置（确切的位置取决于设置），无论在何处运行 `npm install -g <package-name>`。  

在代码中，应该只引入本地的软件包：  
```javascript
require('package-name')
```
通常，所有的软件包都应本地安装。这样可以确保计算机中可以有数十个应用程序，并且如果需要，每个应用程序都可以运行不同的版本。所有的项目都有自己的软件包本地版本，即使这看起来有点浪费资源，但与可能产生的负面影响相比也很小。  

当程序包提供了可从 shell（CLI）运行的可执行命令、且可在项目间复用时，则该程序包应被全局安装。也可以在本地安装可执行命令并使用 npx 运行，但是某些软件包最好在全局安装。  

一些流行的全局软件包的示例有：  
* `node`  
* `vue create`  

可能已经在系统上安装了一些全局软件包。 可以通过在命令行上运行以下命令查看：  
```console
npm list -g --depth 0
```

### 4.11 npm 依赖与开发依赖  

当使用 `npm install <package-name>` 安装 npm 软件包时，是将其安装为依赖项。  


该软件包会被自动地列出在 package.json 文件中的 `dependencies` 列表下（在 npm 5 之前：必须手动指定 `--save`）。  

当添加了 `-D` 或 `--save-dev` 标志时，则会将其安装为开发依赖项（会被添加到 devDependencies 列表）。  

开发依赖是仅用于开发的程序包，在生产环境中并不需要。 例如测试的软件包、webpack 或 Babel。  

当投入生产环境时，如果输入 `npm install` 且该文件夹包含 package.json 文件时，则会安装它们，因为 npm 会假定这是开发部署。  

需要设置 `--production` 标志（`npm install --production`），以避免安装这些开发依赖项。

## 5. Node.js 包运行器 npx  
npx 是一个非常强大的命令，从 npm 的 5.2 版本（发布于 2017 年 7 月）开始可用。  

npx 可以运行使用 Node.js 构建并通过 npm 仓库发布的代码。  

### 5.1 轻松地运行本地命令  

Node.js 开发者过去通常将大多数可执行命令发布为全局的软件包，以使它们处于路径中且可被立即地执行。  

这很痛苦，因为无法真正地安装同一命令的不同版本。  

运行 npx commandname 会自动地在项目的 node_modules 文件夹中找到命令的正确引用，而无需知道确切的路径，也不需要在全局和用户路径中安装软件包。  

### 5.2 无需安装的命令执行  

`npx` 的另一个重要的特性是，无需先安装命令即可运行命令。  

这非常有用，主要是因为：  
 1. 不需要安装任何东西。
 2. 可以使用 @version 语法运行同一命令的不同版本。  

使用 `npx` 的一个典型演示是使用 `cowsay` 命令。 `cowsay` 会打印一头母牛，并在命令中说出你写的内容。 例如：  


```console
npx cowsay "你好"
```
```console
 ______
< 你好 >
 ------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

只有之前已从 `npm` 全局安装了 `cowsay` 命令，才可以这样做，否则，当尝试运行该命令时会获得错误。

`npx` 可以运行该 `npm` 命令，而无需在本地安装：  

这是一个有趣但无用的命令。 其他场景有：  

* 运行 `vue CLI` 工具以创建新的应用程序并运行它们：`npx vue create my-vue-app`。  
* 使用 `create-react-app` 创建新的 React 应用：`npx create-react-app my-react-app`。

#### 5.3 使用不同的 Node.js 版本运行代码  

使用 `@` 指定版本，并将其与 `node npm` 软件包 结合使用：  

```console
npx node@10 -v #v10.18.1
npx node@12 -v #v12.14.1
```
直接从 URL 运行任意代码片段  

npx 并不限制使用 npm 仓库上发布的软件包。  

```console
npx https://gist.github.com/zkat/4bc19503fe9e9309e2bfaa2c58074d32
```
当然，当运行不受控制的代码时，需要格外小心，因为强大的功能带来了巨大的责任。  


### 6. Node.js 事件循环  

事件循环是了解Node.js最重要的方面之一。  
它阐明了Node.js如何做到异步且具有非阻塞的I/O，所以它基本上阐明了Node.js的“杀手级应用”，正是这一点使它成功了。  

Node.js javascript代码运行在单个线程上。每次只处理一件事。这个限制实际上非常有用，因为这大大简化了编程方式，而不必担心并发问题。只需要注意如何编写代码，并避免任何可能阻塞线程的事情，例如同步的网络调用或无限的循环。通常，在大多数浏览器中，每个浏览器选项卡都有一个事件循环，以使每个进程都隔离开，并避免使用无限的循环或繁重的处理来组织整个浏览器的网页。该环境管理多个并发事件循环，例如处理API调用。Web工作进程也运行在自己的事件循环中。  
我们主要需要关心的是代码会在单个事件循环上运行，在编写代码的时候牢记这一点，以避免阻塞它。  

#### 6.1 阻塞事件循环  

任何花费太长时间才能将控制权返回给事件循环的 JavaScript 代码，都会阻塞页面中任何 JavaScript 代码的执行，甚至阻塞 UI 线程，并且用户无法单击浏览、滚动页面等。  

JavaScript 中几乎所有的 I/O 基元都是非阻塞的。 网络请求、文件系统操作等。 被阻塞是个异常，这就是JavaScript 如此之多基于回调（最近越来越多基于 promise 和 async/await）的原因。  

#### 6.2 调用堆栈  

调用堆栈是一个 LIFO 队列（后进先出）。  
事件循环不断地检查调用堆栈，以查看是否需要运行任何函数。  

当执行时，它会将找到的所有函数调用添加到调用堆栈中，并按顺序执行每个函数。  

```javascript
const bar = () => console.log('bar')

const baz = () => console.log('baz')

const foo = () => {
  console.log('foo')
  bar()
  baz()
}

foo()
```
此代码会如预期地打印：  

```console
foo
bar
baz
```
当运行此代码时，会首先调用 foo()。 在 foo() 内部，会首先调用 bar()，然后调用 baz()。  

#### 6.3 了解 process.nextTick()  

























<font color=red></font>
<font color=green></font>
<font color=orange></font>
<b></b>