// 搭建一个HTTP服务器
// 引入http模块
const http = require('http')
// 输出着色模块
const chalk = require('chalk')
// 获取主机地址
const ipAddress = require('../config/get-ip')
// 引入url模块
const url = require('url')

// 设置端口号
const port = 3000
// 本地主机地址
const localHostname = "localhost"
// 网络主机地址(一般也是当前主机的地址)
const networkHostName= ipAddress;
// 创建服务实例
const server = http.createServer((req, res) => {
    console.log('get request success. url:' + req.url)

    res.statusCode = 200
    res.setHeader('Content-Type', 'text/plain')
    res.end('server run success\n' + req.url)
})

// 设置一个监听器
server.listen(port, () => {
    const localUrl = `http://${localHostname}:${port}/`
    const NetUrl = `http://${networkHostName}:${port}/\n`
    console.log('App run at:')
    console.log(`-Local: ` + chalk.blue(localUrl))
    console.log(`-Network: ` + chalk.blue(NetUrl))
})

// 公开server实例
module.exports = server