// 引入依赖模块
const http = require('http')
const url = require('url')
// 输出着色模块
const chalk = require('chalk')
// 获取主机地址
const ipAddress = require('../config/get-ip')
// 设置端口号
const port = 3000
// 本地主机地址
const localHostname = "localhost"
// 网络主机地址(一般也是当前主机的地址)
const networkHostName= ipAddress;


const start = (route) => {
    const onRequest = (req, res) => {
        // 请求url
        let pathname = url.parse(req.url).pathname
        // 调用路由模块
        route(pathname)
        res.writeHead(200, {'Content-Type':'text/plain'})
        res.write('hello world')
        res.end()
    }

    let server = http.createServer(onRequest)
    server.listen(port, () => {
        const localUrl = `http://${localHostname}:${port}/`
        const NetUrl = `http://${networkHostName}:${port}/\n`
        console.log('App run at:')
        console.log(`-Local: ` + chalk.blue(localUrl))
        console.log(`-Network: ` + chalk.blue(NetUrl))
    })
}
// 放开访问权限
exports.start = start