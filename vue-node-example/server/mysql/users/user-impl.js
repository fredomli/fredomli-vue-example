// 用户表数据增删改产操作

// 获取数据库连接
const connection = require('../../config/mysql-conn')

// 向users表中插入数据
// const sql = 'insert into users(id, username, password, address) values(0,?,?,?)'
// const data = ['李四', '123456', '四川宜宾']
// connection.query(sql, data, (err, result) => {
//     console.log('success')
//     if (err) {
//         console.log('数据插入失败')
//         return
//     }
//     console.log('-------------------')
//     console.log(result)
//     console.log('-------------------\n')
// })

// 查询数据
const selectSql = 'select * from users'
connection.query(selectSql, (err, result) => {
    console.log('success')
    if (err) {
        console.log('数据查询失败')
        return
    }
    console.log('-------------------')
    console.log(result)
    console.log('-------------------\n')
})

// 更新数据
// const updateSql = 'update users set username = ? where id = ?'
// const updataData = ['王五',2]
// connection.query(updateSql, updataData, (err, result) => {
//     if (err) {
//         console.log('数据更新失败')
//         return
//     }
//     console.log('-------------------')
//     console.log(result)
//     console.log('-------------------\n')
// })

// 删除数据
const deleteSql = 'delete from users where id = ?'
const deleteData = [2]
connection.query(deleteSql, deleteData, (err, result) => {
    if (err) {
        console.log('数据删除失败')
        return
    }
    console.log('-------------------')
    console.log(result)
    console.log('-------------------\n')
})
// 关闭连接
connection.end()