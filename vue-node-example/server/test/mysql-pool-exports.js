const pool = require('../config/mysql-pool')

console.log('111')

// 连接池测试-增加数据
pool.getConnection((err, conn) => {
    if (err) {
        console.log('和mysql数据库建立连接失败')
        return
    }
    // 增加数据测试
    const sql = 'insert into node_table(id,name,address) values(0,?,?)'
    const data = ['杂货商城', 'http://www.fredom.com']
    conn.query(sql, data, (err, result) => {
        if (err) {
            console.log('数据查询失败')
            return
        }
        console.log('-------------------')
        console.log(result)
        console.log('-------------------\n')
        // 关闭连接池 pool.releaseConnection()，归还连接
        pool.end()
    })
})