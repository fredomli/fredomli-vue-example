// 引入 jwt token工具
const JwtUtil = require('../config/jwt')

// 生成token
const username = 'zhangsan'
// 获取jwt对象
const jwt = new JwtUtil(username)
// 生成token
const token = jwt.generateToken()
// 打印出token
console.log(token)

// 校验token
const jwt2 = new JwtUtil(token)
const result = jwt2.verifyToken()
console.log('---------校验结果----------')
console.log(result)
