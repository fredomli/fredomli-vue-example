// 导入mongo驱动
const MongoClient = require('mongodb').MongoClient
// 获取当前主机地址
// const ipAddress = require('./get-ip')
// 数据库连接地址
// const url = `mongodb://${ipAddress}:27017/webnode`
// 从db文件中获取连接参数
// const mongodb = require('./db').application.mongodb
// const url = `mongodb://${mongodb.host}:${mongodb.port}/${mongodb.database}`
const url = 'mongodb://localhost:27017/webnode'

// 连接数据库
MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
    if (err) throw err;
    console.log("数据库已创建!");
    db.close();
})