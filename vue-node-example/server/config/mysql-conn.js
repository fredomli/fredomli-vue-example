// 引入MySQL模块
const mysql = require('mysql')
// 获取当前主机地址
// const ipAddress = require('./get-ip')
// 获取数据库连接参数
const mysqlConfigValue = require('./db').application.mysql

// 创建一个mysql连接
const connection = mysql.createConnection({
    host: mysqlConfigValue.host,
    user: mysqlConfigValue.user,
    password: mysqlConfigValue.password,
    database: mysqlConfigValue.database

})

// 连接数据库
connection.connect()

// const sql = 'insert into users(id, username, password, address) values(0,?,?,?)'
// const data = ['张三', '123456', '四川达州']
// conn.query(sql, data, (err, result) => {
//     console.log('success')
//     if (err) {
//         console.log('数据查询失败')
//         return
//     }
//     console.log('-------------------')
//     console.log(result)
//     console.log('-------------------\n')
// })

// 增加数据测试
// const sql = 'insert into node_table(id,name,address) values(0,?,?)'
// const data = ['淘宝商城', 'http://www.taobao.com']
// connection.query(sql, data, (err, result) => {
//     if (err) {
//         console.log('数据查询失败')
//         return
//     }
//     console.log('-------------------')
//     console.log(result)
//     console.log('-------------------\n')
// })

// 关闭连接
// connection.end()
console.log('database connection success')


// 公开数据库连接
module.exports = connection