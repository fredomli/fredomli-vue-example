// jwt配置文件
// 引入模块依赖 fs文件系统模块 path路径模块 jsonwebtoken模块
const fs = require('fs')
const path = require('path')
const jsonwebtoken = require('jsonwebtoken')


// 创建token类
class Jwt {
    // 数据
    constructor(data) {
        this.data = data
    }
    // 生成token
    generateToken() {
        // 加密数据
        let data = this.data
        // 当前时间
        let created = Math.floor(Date.now() / 1000)

        // 过期时间
        let endTime = created + 60 * 30
        // 私钥
        let cert = fs.readFileSync(path.join(__dirname, './pem/private_key.pem'))

        // 创建token
        let token = jsonwebtoken.sign({
            data,
            exp: endTime
        }, cert, { algorithm: 'RS256' })

        // 返回token
        return token
    }
    // 校验token
    verifyToken() {
        let token = this.data
        // 公钥
        let cert = fs.readFileSync(path.join(__dirname, './pem/public_key.pem'))
        // 返回值
        let res

        // 校验
        try {
            let result = jsonwebtoken.verify(token, cert, { algorithms: ['RS256'] }) || {}
            let { exp = 0 } = result
            let current = Math.floor(Date.now() / 1000)
            if (current <= exp) {
                res = result.data || {};
            }
        } catch (e) {
            res = 'err';
        }
        return res
    }
}

// 公开jwt
module.exports = Jwt