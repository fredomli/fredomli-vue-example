// 通过连接池来连接数据库
// 引用数据库驱动
const mysql = require('mysql')

// 创建数据库连接池
const pool = mysql.createPool({
    host: 'localhost',
    port: '3306',
    database: 'webnode',
    user: 'root',
    password: '123456'
})

// 封装MysqlPoolGetConn函数，以便外部调用
module.exports = pool
// 连接池测试-增加数据
// pool.getConnection((err, conn) => {
//     if (err) {
//         console.log('和mysql数据库建立连接失败')
//         return
//     }
//     // 增加数据测试
//     const sql = 'insert into node_table(id,name,address) values(0,?,?)'
//     const data = ['小米商城', 'http://www.mi.com']
//     conn.query(sql, data, (err, result) => {
//         if (err) {
//             console.log('数据查询失败')
//             return
//         }
//         console.log('-------------------')
//         console.log(result)
//         console.log('-------------------\n')
//         // 关闭连接池 pool.releaseConnection()，归还连接
//         pool.end()
//     })
// })