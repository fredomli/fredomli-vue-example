/* 该文件是一个全局的参数文件 */

// 公开数据
exports.application = {
    // mysql连接参数
    mysql: {
        host: 'localhost',
        user: 'root',
        password: '123456',
        database: 'webnode'
    },
    // 数据库连接池参数
    mysqlpool: {
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: '123456',
        database: 'webnode'
    },
    // mongodb连接参数
    mongodb: {
        host: 'localhost',
        port: '27017',
        database: 'webnode'
    }
}



/* 文件公开功能两个方式 
const car= {
    brand: 'value1',
    model: 'value2
}
module.exports = car

export.car = car

第一种方式公开指向的对象 第二种公开指向的属性
*/