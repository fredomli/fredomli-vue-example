Vue 教程
========

## 1. 介绍

### 1.1 Vue是什么  

Vue是一套用于构建用户界面的**渐进式框架**.与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。另一方面，当与现代化的工具链以及各种支持类库结合使用时，Vue 也完全能够为复杂的单页应用提供驱动。  
### 1.2 安装
尝试Vue.js最简单的方法是是使用<b style="color:green;">Hello world</b>例子，可以直接在浏览器标签页中打开它跟着例子学习一些基础语法。或者通过<b style="color:green;">创建一个.html文件</b>，然后通过一下方式引入Vue:  
```html
<!-- 开发环境版本包含了帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```

或者：  
```html
<!-- 生产环境版本，优化了尺寸和速度 -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```
## 2. Vue实例

### 2.1 创建一个Vue实例  

每一个Vue应用都是通过用 <b style="color:red;">`Vue` </b>函数创建一个新的**Vue实例**开始的： 
```javascript
var vm = new Vue({
    //选项
})
```

虽然没有完全遵守<span style="color:green;">MVVM模型</span>，但是Vue的设计也受到了它的启发。因此在文档中经常会使用<span style="color:red;">`vm`</span>(ViewModel的缩写) 这个变量表示Vue实例。  

当创建一个Vue实例时，你可以传入一个**选项对象**。 

一个Vue应用由一个通过<span style="color:orange;">`new Vue`</span> 创建的**根Vue实例**,以及可选的嵌套，可复用的组件树组成。举个栗子，一个todo应用的组件树可以是这样的：  

<div style="background:	Gainsboro; height:200px">
Root  

└─ TodoList  
----├─TodoItem  
----│  ├─ TodoButtonDelete  
----│  └─ TodoButtonEdit  
----└─ TodoListFooter  
-------├─ TodosButtonClear  
-------└─ TodoListStatistics  
</div>

### 2.2 数据与方法  

当一个Vue实例被创建时，它将`data`对象中的所有的property加入到Vue的**响应式系统**中，当这些property的值发生改变时，视图将会产生“响应”,即匹配更新为新的值。  
```javascript
//我们的数据对象
var data = {a:1};

// 对象被加入到一个Vue实例中
var vm = new Vue({
    data: data;
})

//获取这个实例上的 property
//返回源数据中对应的字段
vm.a == data.a // => true

//设置property也会影响到原始数据
vm.a = 2
data.a // => 2

//反之亦然
data.a = 3
vm.a // => 3
```
当这些数据改变时，视图会重新渲染。值得注意的是只有当实例被创建时就已经存在与`data`中的property才是**响应式**的，也就是说添加一个新的property，比如：  

```javascript
vm.b = '4'

```

那么对于`b`的改动将不会触发任何视图的更新。如果你知道你会在晚些时候需要一个property，但是一开始它为空或者不存在，那么你就需要设置一些初始值。比如：  
```json
data: {
    newTodoText: '',
    visitCount: 0,
    hideCompletedTodos: false,
    todos: [],
    error: null
}

```

这里唯一的例外是使用`Object.freeze()`,这会阻止修改现有的property，也意味着系统响应无法在最终变化。
```javascript
var obj = {
    foo: 'bar'
}

Object.freeze()

new Vue({
    el: '#app',
    data: obj
})
```

```html
<div id="app">
    <p>{{ foo }}</p>
    <!-- 这里`foo` 不会再更新！ -->
    <button v-on:click="foo = 'baz'">Change it</button>
</div>
```

除了数据property，Vue还暴露了一些有用的实例property与方法。他们都有前缀<span style="color:red;">`$`</span>,以便与用户定义的property区分开来。例如：
```javascript
var data = { a: 1 }
var vm = new Vue({
    el: '#example',
    data: data
})

vm.$data === data // => true
vm.$el === document.getElementById('example') // => true

//$watch 是一个实例方法
vm.$watch('a', function(newValue,oldValue){
    //这个回调将在`vm.a`改变后调用
})
```

### 2.3 实例生命周期钩子

每个Vue实例在被创建时都要经过一系列初始化过程--例如，例如需要设置数据监听、编译模板、将实例挂载到DOM并在数据变化时更新DOM等。  
同时在这个过程中也会运行一些叫做**生命周期钩子**的函数，这给了用户在不同阶段添加自己代码的机会。  
比如<span style="color:red;">`created`</span> 钩子可以用来在一个实例被创建之后执行代码：  
```javascript

new Vue({
    data: {
        a: 1
    },
    created: function(){
        // 'this'指向 vm 实例
        console.log('this is' + this.a)
    }
})
```

也有一些其它钩子，在实例生命周期的不同阶段被调用，如 <span style="color:red">`mounted`</span>、<span style="color:red;">`updated`</span>和<span style="color:red;">`destroyed`</span>。生命周期钩子的`this`上下文指向调用它的Vue实例。  


> 不要在选项property或回调上使用<span style="color:green;">箭头函数</span>,比如  
> `created:() => console.log(this.a)`或`vm.$watch('a', newValue => this.myMethod())`。因为箭头函数并没有`this`，`this`会作为变量一直向上级词法作用域查找，直至找到为止，经常导致：  
> `Uncaught TypeError: Cannot read property of undefined`或  
> ``  


 