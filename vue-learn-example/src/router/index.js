import Vue from 'vue' // 引入vue模块
import VueRouter from 'vue-router'  // 引入路由模块

// 配置路由指向的组件
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import User from '../components/user/User.vue'
import Roles from '../components/roles/Roles.vue'
import List from '../components/goods/List.vue'
import Order from '../components/order/Order.vue'
import Add from '../components/goods/Add.vue'
import Edit from '../components/goods/Edit.vue'
import Params from '../components/goods/Params.vue'
import Rights from '../components/roles/Rights.vue'
import Report from '../components/report/Report.vue'
import Cate from '../components/goods/Cate.vue'
import ReportDataSet from '../components/report/ReportDataSet.vue'
import ReportDataZoom from '../components/report/ReportDataZoom.vue'
Vue.use(VueRouter)  // 将路由模块引入vue模块

// 路由路径
const routes = [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    {
        path: '/home',
        component: Home,
        redirect: '/Welcome',
        children: [
            {path: '/home', component: Welcome},
            {path: '/user', component: User},
            {path: '/roles', component: Roles},
            {path: '/rights', component: Rights},
            {path: '/product', component: List},
            {path: '/product/add', component: Add},
            {path: '/product/edit', component: Edit},
            {path: '/params', component: Params},
            {path: '/cate', component: Cate},
            {path: '/order', component: Order},
            {path: '/report', component: Report},
            {path: '/report-data-set', component: ReportDataSet},
            {path: '/report-data-zoom', component: ReportDataZoom}
        ]
    },

]

// 将配置好的路由路径进行配置
const router = new VueRouter({ routes })

// 挂载路由导航守卫，to表示将要访问的路径，from表示从哪里来，next是下一步需要做的操作 next('/login') 强制跳转登录页面

router.beforeEach((to, from, next) => {
    // 访问登录页面,放行
    if (to.path === '/login') return next()
    // 获取 token
    const token = window.sessionStorage.getItem('token')
    // 没有 token强制跳转到登录页面
    if (!token) return next('/login')
    next()
})

// 公开需要其他能够访问的模块 这里我们将路由访问模块公开
export default router