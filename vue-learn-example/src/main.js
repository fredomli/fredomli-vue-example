import Vue from 'vue' // 引入vue模块
import App from './App.vue' // 引入app组件
import router from './router'  // 引入路由组件
import axios from 'axios' //引入Axios

// 导入NProgress, 包对应的JS和CSS
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 导入element配置信息  
import './plugins/element' 
// 引入element-ui样式文件
import 'element-ui/lib/theme-chalk/index.css' 
// 引入element-ui额外样式
import 'element-ui/lib/theme-chalk/display.css';
// 导入全局样式
import './assets/css/global.css' 
// 导入字体图标
import './assets/fonts/iconfont.css'



// 配置请求根路径，包括本机地址、远程地址
// axios.defaults.baseURL= 'http://localhost:8888/'
axios.defaults.baseURL = 'http://www.test.com/'

// 在request拦截器中，展示进度条Nprogress.start()
// 请求在到达服务器之前，先会调用use这个回调函数来添加请求头信息
axios.interceptors.request.use( config => {
  NProgress.start()
  // 为请求头对象，添加token验证的 Authorization 字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 返回config
  return config
})

// response 拦截器中，隐藏进度条 NProgress.done()
axios.interceptors.response.use( config => {
  NProgress.done()
  return config
})
// 将axios挂载到Vue实例，后面可通过this调用
Vue.prototype.$http = axios
Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
