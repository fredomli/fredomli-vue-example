# fredomli-vue-example

## 介绍  
`fredomli-vue-example`是一个与`Vue`相关的代码仓库，该仓库中包含了大量`Vue`生态技术相关代码以及使用`Vue生态`和`周边技术`开发的各种项目，例如使用`Vuepress`开发的<b style="color:green">`博客系统`</b>、<b style="color:green">`文档系统`</b>，使用`Vue`和`Element-ui`开发的各种类型的<b style="color:green">`管理系统`</b>和<b style="color:green">`Web网站`</b>各个项目主要的目的是学习`Vue`相关的技术和周边生态技术。
## 项目结构  

仓库项目：    

|       项目名称       |      使用技术      |
| :-----------------: |    :-----------:   |
| vue-learn-example   |   `vue` `···`        |
| vue-node-example |  `node` `npm`   `express` |
|fredomli-vuepress-starter| `vue` `vuepress`|
| fredomli-vuepress-blog|   `vue` `vuepress`    |
|vue-webpack-example |  `vue` `webpack`   |
**fredomli-vuepress-blog**  

该项目是使用`Vuepress`开发的一个博客系统。  

**fredomli-vuepress-starter**  

该项目是`Vuepress`学习项目，通过该项目学习`Vuepress`的相关技术。  

**vue-learn-example**  

该项目是`Vue`的学习项目.  

**vue-node-example**

该项目是`Node`的学习项目，该项目用来学习`Node`的基础技术，通过该项目入门`Node`。  

**vue-webpack-example**   

该项目用来学习项目管理工具`Webpack`。 